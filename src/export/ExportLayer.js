import $ from "jquery";
import { LoadingAnimation } from "./LoadingAnimation";

export class ExportLayer
{
    constructor(fetcher, wrkspace)
    {
        this.fetcher = fetcher;
        this.workspace = wrkspace;

        this.form = document.getElementById("form-export-layer");
        this.fileToUpload = document.getElementById("file-to-upload");
        this.uploadButton = document.getElementById("button-upload");
        this.loadingImg = new LoadingAnimation();

        // Fill Themes name with fetched theme
        const themeName = $("#theme-name");
        this.fetcher.getThemes(this.workspace).then(fetcher =>
        {
            Object.keys(fetcher.layerHierarchy[this.workspace]).forEach(theme =>
            {
                $("#select-theme-name")
                    .append(`<option value="${theme}">${theme}</option>`);
            });
        });

        // Toogle divs corresponding to append layers to theme or create new theme
        const createNewThemeCheckBox = $("#new-theme");
        createNewThemeCheckBox.change(() => toogleElement(createNewThemeCheckBox.is(":checked")));
        toogleElement(createNewThemeCheckBox.is(":checked"));


        // When file is selected and if theme name is empty, set theme name to file name
        this.fileToUpload.onchange = event =>
        {
            const themeName = document.getElementById("input-new-theme-name");
            if (themeName.value === "")
            {
                const file = this.fileToUpload.files[0];
                const filenamePart = file.name.split('.');
                themeName.value = filenamePart.slice(0, -1).join('');
            }
        };

        this.form.onsubmit = event =>
        {
            event.preventDefault();
            $("#loading-container").empty();

            this.loadingImg.appendToElementWithId("loading-container");

            if (createNewThemeCheckBox.is(":checked"))
            {
                this.createNewTheme($("#input-new-theme-name").val());
            }
            else
            {
                this.createNewTheme($("#select-theme-name").val());
            }
        };
    }

    createNewTheme(themeName)
    {
        const file = this.fileToUpload.files[0];
        const filenamePart = file.name.split('.');
        const formData = new FormData();
        formData.append("file", file, file.name);

        $.ajax(
        {
            url: `/geoserver/rest/workspaces/${this.workspace}/datastores/${themeName}/file.shp?configure=all`,
            method: "PUT",
            data: formData,
            contentType: "application/zip",
            processData: false
        })
            .done(event =>
            {
                $("#loading-container").append(`<div class="alert alert-success" role="alert">Thème ajouté</div>`);
                this.fetcher.update();
            })
            .fail(event =>
            {
                $("loading-container").append(`<div class="alert alert-danger" role="alert">Problème lors de l'ajout du thème</div>`);
            })
            .always(() =>
            {
                this.loadingImg.remove();
            });
    }
}

function toogleElement(checked)
{
    const newThemeName = $("#new-theme-name");
    const themeName = $("#theme-name");
    if (checked)
    {
        themeName.hide();
        newThemeName.show();
    }
    else
    {
        themeName.show();
        newThemeName.hide();
    }
}
