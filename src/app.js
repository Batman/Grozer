import $ from "jquery";
import Map from "ol/map";
import Tile from "ol/layer/tile";
import OSM from "ol/source/osm";
import View from "ol/view";
import LayerVector from "ol/layer/vector";
import SourceVector from "ol/source/vector";
import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";
import { hexToRGB_HSL } from "./utils/Color";

// import "popper.js/dist/popper.min";
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/style.css';
import 'bootstrap/dist/js/bootstrap.min';

import { ThemeFetcher } from "./layers/ThemeFetcher";
import { LayersMap } from "./layers/LayersMap";
import { intersection } from "./spatial/intersection";
import { equalRange } from "./thematic/equalRange";
import { standardized } from "./thematic/standardized";

import { SortableLayer } from "./ui/SortableLayer";
import { ThemeSwitcher } from "./ui/ThemeSwitcher";

import { ExportLayer } from "./export/ExportLayer";

$(() =>
{
    const CURRENT_WORKSPACE = "topp";
    // Displayed map
    let map = new Map({
        layers: [
            new Tile({
                source: new OSM()
            }),
        ],
        target: 'map',
        view: new View({
            center: [0, 0],
            zoom: 2
        })
    });

    // Contains information of displayed layers
    let layersMap = new LayersMap();

    // Fetch layers informations
    let fetcher = new ThemeFetcher();
    // Fetch workspaces and init the view with the TOPP workspace
    // TODO load the main workspace of the connected user
    fetcher.update = () =>
        fetcher.getWrkspaces().then(fetcher =>
        {
            new ThemeSwitcher(map, layersMap, fetcher, CURRENT_WORKSPACE);
            new ExportLayer(fetcher, CURRENT_WORKSPACE);
        });
    fetcher.update();

    $("#intersection").click(() =>
    {
        if (layersMap.containsKey('intersection'))
        {
            map.getLayers().remove(layersMap.intersection);
            layersMap.delete('intersection');
            // layersMap['intersection'] = undefined;
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let intersectionLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            intersection(layers.item(layersLength -1).getSource().getFeatures(),
                         layers.item(layersLength -2).getSource().getFeatures())
                .forEach(inter => intersectionLayer.getSource().addFeature(inter));
            map.getLayers().push(intersectionLayer);
            layersMap.put('intersection', intersectionLayer);
        }
    });

    $("#equal-range").click(() =>
    {
        if (layersMap.containsKey('equalRange'))
        {
            map.getLayers().remove(layersMap.equalRange);
            // layersMap['equalRange'] = undefined;
            layersMap.delete('equalRange');
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let layer = layers.item(layersLength -1);
            let features = layer.getSource().getFeatures();

            let equalRangeLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            //Coloring type: Monochromatic, Polychromatic, Manual
            let colortype;
            if ($("input[name=colortype]:checked").val() === "manuel")
            {
                colortype = getSelectedColors();
            }
            else
            {
                colortype = $("input[name=colortype]:checked").val();
            }
            equalRange($("#input_nb_classes_color").val(), features, $("#featuresAttributes").val(), colortype)
                .forEach(ft => equalRangeLayer.getSource().addFeature(ft));
            map.getLayers().push(equalRangeLayer);
            layersMap.put('equalRange', equalRangeLayer);
        }
    });

    $("#standardized").click(() =>
    {
        if (layersMap.containsKey('standardized'))
        {
            map.getLayers().remove(layersMap.standardized);
            // layersMap['standardized'] = undefined;
            layersMap.drop('standardized');
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let layer = layers.item(layersLength -1);
            let features = layer.getSource().getFeatures();

            let standardizedLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            //Coloring type: "Monochromatic", "Polychromatic", or manual: pass array of colors instead
            let colortype;
            if ($("input[name=colortype]:checked").val() === "manuel")
            {
                colortype = getSelectedColors();
            }
            else
            {
                colortype = $("input[name=colortype]:checked").val();
            }
            standardized($("#input_nb_classes_color").val(), features, $("#featuresAttributes").val(), colortype)
                .forEach(ft => standardizedLayer.getSource().addFeature(ft));
            map.getLayers().push(standardizedLayer);
            layersMap.put('standardized', standardizedLayer);
        }
    });

    // Initialise layer list
    let sortableLayers = new SortableLayer(map, layersMap, fetcher);

    $("#button-export-current-layers").on("click", function()
    {
        $("#popup-export-list-couche-selected").empty();
        $("#list-couche-selected").clone().appendTo("#popup-export-list-couche-selected");
    });

    $("#input_nb_classes_color").on("change", function()
    {
        let val = this.value;
        let old = this.dataset.old;

        if(old == -1)
            for(let i = 1; i<= val; i++)
                $("<input>").attr({type:"color",id:`classe_color_${i}`, name:"manuel", style:"margin-right: 4px"}).appendTo("#list_color_class");
        else
        {
            if(val > old)
                $("<input>").attr({type:"color",id:`classe_color_${val}`, name:"manuel", style:"margin-right: 4px"}).appendTo("#list_color_class");
            else
                $("#list_color_class").find( `#classe_color_${old}` ).remove();
        }

        this.dataset.old = val;

        console.log(getSelectedColors());
    });

    $("input[name=colortype]", '#analyze-form').on("change", function()
    {
        console.log(this);
        console.log(this.value);

        if(this.value == "manuel")
        {
            $("#input_nb_classes_color").prop("disabled", false);
            $("input[name=manuel]").prop("disabled", false);
        }
        else
        {
            $("#input_nb_classes_color").prop("disabled", true);
            $("input[name=manuel]").prop("disabled", true);
        }

    });


    $("#featuresAttributes").on("focus", function()
    {
        let attrs = getAttributesSelectedFeatures();

        $("#featuresAttributes").empty();

        $("<option>", {value:"", text:""}).appendTo("#featuresAttributes");

        attrs.forEach(function(a)
        {
            a != "geometry" && $("<option>", {value: a, text: a}).appendTo("#featuresAttributes");
        });
    });


    function getAttributesSelectedFeatures()
    {
        let featuresKey = new Set();
        const layers = map.getLayers();

        // console.log("length "+layers.getLength());

        for(let i = 1; i < layers.getLength(); i++)
        {
            let layer = layers.item(i);
            // console.log(layer);

            try
            {
                let features = layers.item(i).getSource().getFeatures();
                features.forEach(function(f)
                {
                    for (let objectKey in f.values_)
                        featuresKey.add(objectKey)
                });
            }
            catch (e) {console.log(e);}
        }

        return featuresKey;
    }

    //return and array of selected colors [{rgb:{r,g,b}, hsl:{h,s,l}}]
    function getSelectedColors()
    {
        // let manual_color = $("input#", '#analyze-form').val(); // get selected mono or poly radio
        let color_selectors = $(`input[name=manuel]`); // get all inputs

        let hexs = [];
        let rgb_hsl = [];

        color_selectors.each(function(i, elem)
        {
            let hex = elem.value;
            hexs.push(hex);
            rgb_hsl.push(hexToRGB_HSL(hex));
        });

        return rgb_hsl
    }
});
