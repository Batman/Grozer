export function colorFeaturesMonochromatic(classesNumber, negativeValues)
{
    //Red; Orange; Yellow; Green; Blue; Purple; Pink;
    let HValues = [0, 30, 60, 120, 240, 280, 300];
    let colors = [];
    //First acceptable value for L: 90 (lighter); Last: 10 (darker); >90 is too white; <10 is too black
    const firstLValue = 85;
    const lastLValue = 15;
    //H (hue) is the color chosen. 0 is red up to 360 which comes full circle so also red.
    let H = 0;
    if (negativeValues)
    {
        H = 240;
    }
    else
    {
        H = 0;
    }
    //S (saturation) could always be 100, changing it is not necessary to get a good range of colors
    const S = 100;
    //L (lightness) dictates the lightness of the color of the first class
    let L = firstLValue;
    //LRange is the difference in L of two colors
    const LRange = Math.floor((firstLValue - lastLValue) / (classesNumber-1));
    for (let i = 0; i < classesNumber; i++)
    {
        let strokeColor = "hsl("+H+", "+S+"%, "+L+"%)";
        let fillColor = "hsla("+H+", "+S+"%, "+L+"%, 0.3)";
        L = L - LRange;
        colors.push(strokeColor);
        colors.push(fillColor);
    }
    return colors;
}

export function colorFeaturesPolychromatic(classesNumber, negativeValues)
{
    //Red; Orange; Yellow; Green; Blue; Purple; Pink;
    let HValues = [0, 30, 60, 120, 240, 280, 300];
	let colors = [];
    let firstHValue = 0;
    let lastHValue = 0;
	//H (hue) is the color chosen. 0 is red up to 360 which comes full circle so also red.
	if (negativeValues)
    {
        firstHValue = 240;
        lastHValue = 120;
    }
    else
    {
        firstHValue = 60;
        lastHValue = 0;
    }
    let H = firstHValue;
	//S (saturation) could always be 100, changing it is not necessary to get a good range of colors
	const S = 100;
	//L (lightness) dictates the lightness of the color of the first class
	const L = 50;
	//HRange is the difference in H of two colors
	const HRange = Math.floor((firstHValue - lastHValue) / (classesNumber-1));
	for (let i = 0; i < classesNumber; i++)
	{
		let strokeColor = "hsl("+H+", "+S+"%, "+L+"%)";
		let fillColor = "hsla("+H+", "+S+"%, "+L+"%, 0.3)";
		H = H - HRange;
		colors.push(strokeColor);
		colors.push(fillColor);
	}
	return colors;
}

export function colorFeaturesManual(colorsPicked)
{

    let colorsArray = rgbToHslArray(colorsPicked);
    for (let i = 0; i < colorsArray.length; i++)
    {
        let tmp = colorsArray[i].substr(0, 3) + "a" + colorsArray[i].substr(3);
        let fillColor = tmp.substr(0, tmp.length-1) + ", 0.3" + tmp.substr(tmp.length-1);
        if (i < colorsArray.length - 1)
        {
            colorsArray.splice(i+1, 0, fillColor);
        }
        else
        {
            colorsArray.push(fillColor);
        }
        i++;
    }
    return colorsArray;
}

export function quickSortFeaturesByValue(items, sortValue, left, right)
{
    var index;
    if (items.length > 1)
    {

        left = typeof left != "number" ? 0 : left;
        right = typeof right != "number" ? items.length - 1 : right;

        index = partition(items, left, right, sortValue);

        if (left < index - 1)
        {
            quickSortFeaturesByValue(items, sortValue, left, index - 1);
        }

        if (index < right)
        {
            quickSortFeaturesByValue(items, sortValue, index, right);
        }
    }

    return items;
}

function partition(items, left, right, sortValue)
{
    var pivot   = items[Math.floor((right + left) / 2)].values_[sortValue],
        i       = left,
        j       = right;

    while (i <= j)
    {
        while (items[i].values_[sortValue] < pivot)
        {
            i++;
        }

        while (items[j].values_[sortValue] > pivot)
        {
            j--;
        }

        if (i <= j)
        {
            swap(items, i, j);
            i++;
            j--;
        }
    }

    return i;
}

function swap(items, firstIndex, secondIndex)
{
    var temp = items[firstIndex];
    items[firstIndex] = items[secondIndex];
    items[secondIndex] = temp;
}

function rgbToHsl(r, g, b)
{
    var min, max, i, l, s, maxcolor, h, rgb = [];
    rgb[0] = r / 255;
    rgb[1] = g / 255;
    rgb[2] = b / 255;
    min = rgb[0];
    max = rgb[0];
    maxcolor = 0;
    for (i = 0; i < rgb.length - 1; i++)
    {
        if (rgb[i + 1] <= min)
        {
            min = rgb[i + 1];
        }
        if (rgb[i + 1] >= max)
        {
            max = rgb[i + 1];maxcolor = i + 1;
        }
    }
    if (maxcolor == 0)
    {
        h = (rgb[1] - rgb[2]) / (max - min);
    }
    if (maxcolor == 1)
    {
        h = 2 + (rgb[2] - rgb[0]) / (max - min);
    }
    if (maxcolor == 2)
    {
        h = 4 + (rgb[0] - rgb[1]) / (max - min);
    }
    if (isNaN(h))
    {
        h = 0;
    }
    h = h * 60;
    if (h < 0)
    {
        h = h + 360;
    }
    l = (min + max) / 2;
    if (min == max)
    {
        s = 0;
    }
    else
    {
        if (l < 0.5)
        {
            s = (max - min) / (max + min);
        }
        else
        {
            s = (max - min) / (2 - max - min);
        }
    }
    s = s;
    return "hsl("+Math.floor(h)+", "+Math.floor(s)+"%, "+Math.floor(l)+"%)";
}

function rgbToHslArray(colors)
{
    let res = [];
    for (let i = 0; i < colors.length; i++)
    {
        // rgbToHsl(r, g, b) function doesn't seem to work, so hsl values used instead
        //res.push(rgbToHsl(colors[i].rgb.r, colors[i].rgb.g, colors[i].rgb.b));
        res.push("hsl("+colors[i].hsl.h+", "+colors[i].hsl.s+"%, "+colors[i].hsl.l+"%)");
    }
    return res;
}