const path = require('path');
const webpack = require("webpack");
const webpackShellPlugin = require('webpack-shell-plugin');

module.exports = {
    entry: './src/app.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                }
            }
        ],
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: /\.(png|jpg|jpeg|svg|gif)$/,
                use: ['file-loader']
            }
        ]
    },
    plugins: [
        // new webpack.optimize.UglifyJsPlugin()
        new webpackShellPlugin({onBuildStart:['echo "Webpack Start"'], onBuildEnd:['python copy-files.py']})
    ]
};
